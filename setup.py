from distutils.core import setup
from maze import __version__

setup(
    name='maze',
    version=__version__,
    packages=['maze', 'maze.cli', 'maze.cli.commands', 'maze.storages'],
    entry_points={
        "console_scripts": [
            "maze = maze.__main__:main",
        ],
    },
    install_requires=[
        'click',
        'PrettyTable',
        'colorama',
        'jsonschema',
    ],
    url='',
    license='',
    author='andreafox',
    author_email='',
    description=''
)
