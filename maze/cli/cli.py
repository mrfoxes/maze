import click

from .commands.play_maze import play_maze
from .commands.show_maps import list_available_maps
from ..storages import storage


@click.group()
def cli():
    """A-Maze-ingly Retro Route Puzzle"""
    pass


@cli.command()
def show_maps():
    """Show all available maps loaded in storage"""
    list_available_maps()


def validate_map(ctx, param, value):
    if not storage.exist(value) and value is not None:
        raise click.BadParameter('Map does not exist\n\nShow available maps via maze show-maps')

    return value


@cli.command()
@click.option('--m', required=False, default=None, callback=validate_map)
@click.option('--random', required=False, default=False, is_flag=True)
@click.option('--start', prompt='From which room do you wanna start?')
@click.option('--collect',
              prompt='Which items do you want to collect in your path? (multiple with a comma)')
def play(m, random, start, collect):
    print(m, random, start, collect)
    if m is None and not random:
        click.secho('You must select a map in order to play', fg='red')
        show_available_maps = click.confirm(
            'Do you want to list available maps?',
            default=True,
            prompt_suffix=': ',
            show_default=True,
            err=False,
        )

        if show_available_maps:
            list_available_maps()

            m = click.prompt('Which map you want to play', type=str)
        else:
            random = click.confirm(
                'Do you want to play a random map?',
                default=True,
                prompt_suffix=': ',
                show_default=True,
                err=False,
                abort=True,
            )

    if random:
        click.secho('Selecting a random map...', fg='green')
        raise NotImplementedError('Must implement select random map')

    play_maze(m, start, collect)
