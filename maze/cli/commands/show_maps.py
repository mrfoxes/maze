from prettytable import PrettyTable

from maze.storages import storage


def list_available_maps():
    x = PrettyTable()

    x.field_names = ['Id', 'Name']

    for m in storage.get_available_maps():
        x.add_row([m, m])

    print(x)
