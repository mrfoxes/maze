import json

import click
import jsonschema

from maze.maze import Maze
from maze.storages import storage


def play_maze(chosen_map: str, start_room: int, collectibles: str):
    if not storage.exist(chosen_map):
        raise Exception('Cannot play a non existing map')

    try:
        with open(str(storage.load_map(chosen_map))) as f:
            maze = Maze(json.load(f))
            maze.prepare()
            maze.collect(start_room, list(map(lambda i: str(i).strip(), collectibles.split(','))))
            click.echo(maze.print_path_table())
    except json.decoder.JSONDecodeError as e:
        click.secho('Map file corrupted, cannot load data.', fg='red')
        raise e
    except jsonschema.exceptions.ValidationError as e:
        click.secho('Cannot load a map with an invalid schema. Please check your map and try again.', fg='red')
        raise e
    except Exception as e:
        click.secho(str(e), fg='red')
