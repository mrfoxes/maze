from collections import namedtuple

import click
from jsonschema import validate
from prettytable import PrettyTable

from maze.utils import map_schema

DIRECTION = namedtuple('DIRECTION', ['north', 'south', 'east', 'west'])
DIRECTIONS = DIRECTION('north', 'south', 'east', 'west')


def validate_map_data(map_data):
    validate(instance=map_data, schema=map_schema)


class MapNotBuiltException(Exception):
    pass


class Maze:
    _map_data: dict = None
    _map = None
    _path = []
    _directions_order = [DIRECTIONS.north, DIRECTIONS.south, DIRECTIONS.west, DIRECTIONS.east]

    def __init__(self, map_data):
        # todo: validate map data struct
        validate_map_data(map_data)
        self._map_data = map_data

    def print_path_table(self):
        x = PrettyTable()

        x.field_names = ['ID', 'Room', 'Object collected']

        for stat in self._path:
            [room_id, room_name, items] = stat
            x.add_row([room_id, room_name, str(', ').join(items) if len(items) > 0 else None])

        return x

    def prepare(self):
        rooms = self._map_data.get('rooms', [])
        self._map = dict((int(r.get('id')), r) for r in rooms)

    def collect(self, start_room: int, items: list):
        if self._map is None:
            raise MapNotBuiltException('Maze must be built in order to play. Prepare the maze before collecting items.')

        valid_items = list(filter(lambda i: i is not None and i != '', items))
        self._walk(int(start_room), valid_items)

    def _walk(self, room_id, items):
        if room_id is None:
            return

        current_room = self._map.get(room_id, None)

        if current_room is None:
            return

        collected_items = self._collect_items(current_room, items)
        self._path.append([current_room.get('id'), current_room.get('name'), collected_items])
        next_room = self._get_next_room(current_room)

        self._walk(next_room, items)

    def _collect_items(self, room, items):
        room_objects = room.get('objects', [])
        found_items = filter(lambda ri: ri.get('name') in items, room_objects)
        remaining_items = filter(lambda ri: ri.get('name') not in items, room_objects)
        room['objects'] = list(remaining_items)
        return list(map(lambda f: f.get('name'), found_items))

    def _get_next_room(self, room):
        for d in self._directions_order:
            if self._can_move(room, d):
                return self._move_direction(room, d)

        return None

    def _can_move(self, room, direction):
        return room.get(direction, None) is not None

    def _move_direction(self, room, direction):
        return room.pop(direction)
