import os

DATA_DIR = os.getenv('DATA_DIR', '/opt/data')

STORAGE_CLASS = os.getenv('STORAGE_CLASS', 'maze.storages.filesystem.FileSystemStorage')
