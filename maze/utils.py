from jsonschema import validate

map_schema = {
    "type": "object",
    "properties": {
        "rooms": {
            "type": "array",
            "items": {"$ref": "#/definitions/room"}
        }
    },
    "required": ["rooms"],
    "definitions": {
        "object": {
            "type": "object",
            "properties": {
                "name": {"type": "string"}
            },
            "required": ["name"]
        },
        "room": {
            "type": "object",
            "properties": {
                "id": {"type": "number"},
                "name": {"type": "string"},
                "north": {"type": "number"},
                "south": {"type": "number"},
                "west": {"type": "number"},
                "east": {"type": "number"},
                "objects": {"type": "array"},
            },
            "required": ["id", "name"],
        }
    }
}
