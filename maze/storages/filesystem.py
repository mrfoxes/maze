import pathlib

from maze.settings import DATA_DIR
from maze.storages.storage import Storage


class FileSystemStorage(Storage):
    def exist(self, map_id):
        return map_id in self.get_available_maps()

    def _format_map_name(self, file_path: pathlib.PosixPath):
        return str(file_path).split('/')[-1].split('.')[0]

    def load_map(self, map_id):
        return pathlib.Path(DATA_DIR).joinpath(f'{map_id}.json')

    def get_available_maps(self) -> list:
        files = list(pathlib.Path(DATA_DIR).glob('*.*json'))

        return list(map(self._format_map_name, files))
