docker-build-test:
	docker build -f ./docker/Dockerfile.test -t maze:test .

docker-build-prod:
	docker build -f ./docker/Dockerfile -t maze .

play: docker-build-prod
	docker run -it -v $(shell pwd)/data:/opt/data maze:latest play

play_map_1:
	docker run -it -v $(shell pwd)/data:/opt/data maze:latest play --m map_1 --start 2 --collect "Knife, Potted Plant"

play_map_2:
	docker run -it -v $(shell pwd)/data:/opt/data maze:latest play --m map_2 --start 4 --collect "Knife, Potted Plant, Pillow"
