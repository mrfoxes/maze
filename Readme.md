#Maze

## Description

A simple cli game written in python

### Run

Run game

    docker-compose -f docker-compose.prod.yaml run maze play
    
Run tests

    docker-compose run tests
    
Build and run directly from image:latest

    docker build -f ./docker/Dockerfile -t maze . && docker run -it -v $(pwd)/data:/opt/data maze:latest play
    
#### Running without prompt as exercise

Non docker

    DATA_DIR=$(pwd)/data maze play --m map_1 --start 2 --collect "Knife, Potted Plant"

    DATA_DIR=$(pwd)/data maze play --m map_2 --start 4 --collect "Knife, Potted Plant, Pillow"
    
Docker
    
    docker run -it -v $(shell pwd)/data:/opt/data maze:latest play --m map_1 --start 2 --collect "Knife, Potted Plant"
    
    docker run -it -v $(shell pwd)/data:/opt/data maze:latest play --m map_2 --start 4 --collect "Knife, Potted Plant, Pillow"
    
Docker compose prod

    docker-compose -f docker-compose.prod.yaml run maze play --m map_1 --start 2 --collect "Knife, Potted Plant"
    
    docker-compose -f docker-compose.prod.yaml run maze play --m map_2 --start 4 --collect "Knife, Potted Plant, Pillow"
    
Docker compose test

    docker-compose run maze play --m map_1 --start 2 --collect "Knife, Potted Plant"
    
    docker-compose run maze play --m map_2 --start 4 --collect "Knife, Potted Plant, Pillow"

## How to develop

You can choose to develop inside a local python environment or via Docker compose

### Local development

Setup pipenv

    pipenv install
    
Install cli package in development mode

    pipenv shell && pip install -e .
    
The command **maze** will be available as a python module.

    maze --help

### Docker compose

### Testing

Testing is done via pytest, in order to run tests you can directly lunch the **pytest** command or use the shortcut via **make task**

    pytest
    
You can also test via docker compose, remember to build the image

    docker compose run test
    
## Loading Maps

Maps are loaded via filesystem, in order to load your maps you MUST mount a directory containing json maps under /opt/data.

The game provide an option to load maps via JSON strings via cli.

#### Important

The default directory for the program to find the maps is located at **/opt/data**, and environment variable is provided in order to change the data folder 

    DATA_DIR = os.getenv('DATA_DIR', '/opt/data')
    
    DATA_DIR=/some/dir/in/my/machine maze play

## Run the game

    docekr-compose run maze play
    
## Production image

Build the image **maze:latest**

    make docker-build-prod
