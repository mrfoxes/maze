import json
import pathlib

import jsonschema
import pytest

from maze.maze import validate_map_data

FIXTURES_DIR = pathlib.Path(__file__).parent.absolute().joinpath('fixtures')


def test_valid_map():
    with open(str(FIXTURES_DIR.joinpath('map_valid.json'))) as f:
        validate_map_data(json.load(f))


def test_invalid_map():
    with pytest.raises(jsonschema.exceptions.ValidationError):
        with open(str(FIXTURES_DIR.joinpath('map_invalid.json'))) as f:
            validate_map_data(json.load(f))


def test_corrupt_map():
    with pytest.raises(json.decoder.JSONDecodeError):
        with open(str(FIXTURES_DIR.joinpath('map_corrupt.json'))) as f:
            validate_map_data(json.load(f))
