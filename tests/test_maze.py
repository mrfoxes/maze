import json
import pathlib

import jsonschema
import pytest

from maze.maze import validate_map_data, Maze, MapNotBuiltException

FIXTURES_DIR = pathlib.Path(__file__).parent.absolute().joinpath('fixtures')


def test_maze_load():
    with open(str(FIXTURES_DIR.joinpath('map_valid.json'))) as f:
        Maze(json.load(f))


def test_maze_not_build():
    with pytest.raises(MapNotBuiltException):
        with open(str(FIXTURES_DIR.joinpath('map_valid.json'))) as f:
            maze = Maze(json.load(f))
            maze.collect(4, [])


def test_maze_collect():
    with open(str(FIXTURES_DIR.joinpath('map_valid.json'))) as f:
        maze = Maze(json.load(f))
        maze.prepare()
        maze.collect(4, [])
